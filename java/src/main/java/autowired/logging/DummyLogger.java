package autowired.logging;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public class DummyLogger {

    public static final int LEVEL_OFF = 0;
    public static final int LEVEL_INFO = 2;
    public static final int LEVEL_DEBUG = 3;
    public static final int LEVEL_TRACE = 4;

    private static final String DUMMY_LOGER_PROPERTY_KEY = "logLocally";

    private static final DummyLogger instance = new DummyLogger();

    private int level;
    private DummyLogAppender appender;

    private DummyLogger() {
        appender = getDefaultAppender();
        if (appender instanceof DummyFileLogAppender) {
            level = LEVEL_TRACE;
        } else {
            level = LEVEL_OFF;
        }
    }

    public static DummyLogAppender getDefaultAppender()   {
        DummyLogAppender result = null;
        String propertyValue = System.getProperty(DUMMY_LOGER_PROPERTY_KEY);
        if (Boolean.parseBoolean(propertyValue)) {
            try {
                result = new DummyFileLogAppender();
            } catch (FileNotFoundException | UnsupportedEncodingException e) {}
        }
        if (result == null) {
            result = new DummyLogAppender() {
                @Override
                public void appendInfo(String message) {}
                @Override
                public void appendDebug(String message) {}
                @Override
                public void appendTrace(String message) {}
                @Override
                public void shutdown() {}
            };
        }
        return result;
    }

    public static DummyLogger getInstance() {
        return instance;
    }

    public void setLevel(int logLevel) {
        this.level = logLevel;
    }

    public int getLevel() {
        return level;
    }

    public boolean isLoggingInfo() {
        return level >= LEVEL_INFO;
    }

    public void info(String message) {
        if (level >= LEVEL_INFO && message != null) {
            appender.appendInfo(message);
        }
    }

    public boolean isLoggingDebug() {
        return level >= LEVEL_DEBUG;
    }

    public void debug(String message) {
        if (level >= LEVEL_DEBUG && message != null) {
            appender.appendDebug(message);
        }
    }

    public boolean isLoggingTrace() {
        return level >= LEVEL_TRACE;
    }

    public void trace(String message) {
        if (level >= LEVEL_TRACE && message != null) {
            appender.appendTrace(message);
        }
    }

    public void shutdown() {
        appender.shutdown();
    }

}
