package autowired.logging;

public interface DummyLogAppender {

    public static final String DEFAULT_ENCODING = "UTF-8";

    public void appendInfo(String message);
    public void appendDebug(String message);
    public void appendTrace(String message);
    public void shutdown();

}
