package autowired.logging;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class DummyFileLogAppender implements DummyLogAppender {

    private static final String DEFAULT_FILE_NAME = "autowired";
    private static final String LOG_FILE_INFO = "-info";
    private static final String LOG_FILE_DEBUG = "-debug";
    private static final String LOG_FILE_TRACE = "-trace";
    private static final String LOG_FILE_SUFFIX = ".log";

    private static final String DEFAULT_FILE_NAME_PROPERTY = "logFileName";

    private PrintWriter infoWritter;
    private PrintWriter debugWritter;
    private PrintWriter traceWritter;


    public DummyFileLogAppender() throws FileNotFoundException, UnsupportedEncodingException {
        String fileName = System.getProperty(DEFAULT_FILE_NAME_PROPERTY, DEFAULT_FILE_NAME);
        infoWritter = new PrintWriter(fileName + LOG_FILE_INFO + LOG_FILE_SUFFIX, DEFAULT_ENCODING);
        debugWritter = new PrintWriter(fileName + LOG_FILE_DEBUG + LOG_FILE_SUFFIX, DEFAULT_ENCODING);
        traceWritter = new PrintWriter(fileName + LOG_FILE_TRACE + LOG_FILE_SUFFIX, DEFAULT_ENCODING);
    }

    @Override
    public void appendDebug(String message) {
        debugWritter.println(message);
        debugWritter.flush();
    }

    @Override
    public void appendInfo(String message) {
        infoWritter.println(message);
        infoWritter.flush();
    }

    @Override
    public void appendTrace(String message) {
        traceWritter.println(message);
        traceWritter.flush();
    }

    @Override
    public void shutdown() {
        infoWritter.close();
        debugWritter.close();
        traceWritter.close();
    }

    @Override
    public void finalize() {
        shutdown();
    }

}
