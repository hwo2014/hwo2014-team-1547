package autowired.datamodel.car;

public class CarLane {

    private int startLaneIndex;
    private int endLaneIndex;

    public CarLane() {}

    public CarLane(int startLaneIndex, int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }

}
