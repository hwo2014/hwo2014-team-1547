package autowired.datamodel.car;

public class Car {

    private PlayerId id;
    private Dimensions dimensions;

    public Car(PlayerId id) {
        this.id = id;
    }

    public PlayerId getId() {
        return id;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

}
