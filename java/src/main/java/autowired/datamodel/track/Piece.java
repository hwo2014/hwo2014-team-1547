package autowired.datamodel.track;

import java.util.ArrayList;
import java.util.List;

import autowired.logging.DummyLogger;

import com.google.gson.annotations.SerializedName;

public class Piece {
    private static final double PI_DIV_180 = Math.PI / 180;
    private double length;
    @SerializedName("switch")
    private boolean switchPiece;
    private int radius;
    private double angle;
    private double[] laneLength;

    private List<Boolean> laneChanged = new ArrayList<Boolean>();

    final DummyLogger logger = DummyLogger.getInstance();

    public Piece() {
        super();
    }

    public double getLength() {
        return length;
    }

    public boolean isSwitch() {
        return switchPiece;
    }

    public int getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    public double posToEnd(double pos) {
        return length - pos;
    }
    
    public boolean isLaneChanged(int lap) {
        if (lap < 0 || lap >= laneChanged.size()) {
        }
        if (lap < 0) {
            // dont switch in laps less than zero
            return true;
        }
        if (laneChanged.size() <= lap) {
            laneChanged.add(false);
            return false;
        }
        return laneChanged.get(lap);
    }

    public void setLaneChanged(int lap) {
        laneChanged.add(lap, true);
    }

    /**
     * Indicates whether this piece is straight or band.
     * 
     * @return
     */
    public boolean isStraight() {
        return radius == 0;
    }

    public double getLaneLength(int laneIndex) {
        return laneLength[laneIndex];
    }

    public void initialize(TrackLane[] lanes, int laps) {
        setLanesLength(lanes);
    }

    private void setLanesLength(TrackLane[] lanes) {
        laneLength = new double[lanes.length];
        double absAngle = PI_DIV_180 * Math.abs(angle);

        for (int i = 0; i < lanes.length; i++) {
            TrackLane lane = lanes[i];
            if (isStraight()) {
                laneLength[i] = length;
            } else {
                double signumAngle = Math.signum(angle);
                laneLength[i] = absAngle * (radius - (signumAngle * lane.getDistanceFromCenter()));
            }// TODO calculation for switch
        }
    }

    public double getMaximalSafeSpeed(TrackLane lane, Piece[] pieces, int currentPieceIndex) {
        if (isStraight()) {
            return Double.MAX_VALUE;
        }

        // TODO constants
        double signumAngle = Math.signum(angle);

        double radiusForLane = (radius - (signumAngle * lane.getDistanceFromCenter()));
        double maxSafeSpeed;

        if (radiusForLane < 40) {
            maxSafeSpeed = Math.sqrt(0.30 * radiusForLane);
            } else {
            if (radiusForLane < 60) {
                maxSafeSpeed = Math.sqrt(0.38 * radiusForLane);
            } else if (radiusForLane < 75) {
                maxSafeSpeed = Math.sqrt(0.42 * radiusForLane);
            } else if (radiusForLane < 125) {
                maxSafeSpeed = Math.sqrt(0.44 * radiusForLane);
            } else if (radiusForLane < 225) {
                maxSafeSpeed = Math.sqrt(0.48 * radiusForLane);
            } else {
                maxSafeSpeed = Math.sqrt(0.50 * radiusForLane);
            }
            }

        return maxSafeSpeed;
    }

    public static int settlePieceIndex(int i, Piece[] pieces) {
        if (i >= pieces.length) {
            i -= pieces.length;
        } else if (i < 0) {
            i += pieces.length;
        }
        return i;
    }

}
