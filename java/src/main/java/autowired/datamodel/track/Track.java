package autowired.datamodel.track;

public class Track {

    private String id;
    private String name;
    private Piece[] pieces;
    private TrackLane[] lanes;
    private StartingPoint startingPoint;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Piece[] getPieces() {
        return pieces;
    }

    public TrackLane[] getLanes() {
        return lanes;
    }

    public StartingPoint getStartingPoint() {
        return startingPoint;
    }

    public Piece getPiece(int pieceIndex) {
        return pieces[settlePieceIndex(pieceIndex)];
    }

    private int settlePieceIndex(int pieceIndex) {
        int trackPiecesCount = pieces.length;
        if (pieceIndex >= trackPiecesCount) {
            pieceIndex -= trackPiecesCount;
        }
        return pieceIndex;
    }

}
