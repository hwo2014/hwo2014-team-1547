package autowired.datamodel.track;

public class TrackLane {

    private int distanceFromCenter;
    private int index;

    public int getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }

}
