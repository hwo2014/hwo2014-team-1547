package autowired.datamodel.track;

public enum Direction {
    LEFT("Left"),
    RIGHT("Right");

    private String text;

    private Direction(String text) {
        this.text = text;
    }

    public Direction fromString(String text) {
        Direction result = null;
        for (Direction direction : Direction.values()) {
            if (direction.text.equals(text)) {
                result = direction;
                break;
            }
        }
        return result;
    }

    public String getText() {
        return text;
    }

}
