package autowired.datamodel.track;

import autowired.datamodel.car.CarLane;

public class PiecePosition {

    private int pieceIndex;
    private double inPieceDistance;
    private CarLane lane;
    private int lap;

    public PiecePosition() {}

    public PiecePosition(int pieceIndex, double inPieceDistance, CarLane lane, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public CarLane getLane() {
        return lane;
    }

    public int getLap() {
        return lap;
    }
}
