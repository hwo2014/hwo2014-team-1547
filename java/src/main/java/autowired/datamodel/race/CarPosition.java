package autowired.datamodel.race;

import autowired.datamodel.car.PlayerId;
import autowired.datamodel.track.PiecePosition;

public class CarPosition {

    private PlayerId id;
    private Double angle;
    private PiecePosition piecePosition;

    public CarPosition() {}

    public CarPosition(PlayerId id, Double angle, PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    public PlayerId getId() {
        return id;
    }

    public Double getAngle() {
        return angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

}
