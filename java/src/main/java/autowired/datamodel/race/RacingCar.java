package autowired.datamodel.race;

import autowired.datamodel.car.Car;
import autowired.datamodel.car.PlayerId;
import autowired.datamodel.track.Piece;

import autowired.logging.DummyLogger;

public class RacingCar {

    private CarPosition currentCarPosition;
    private CarPosition previousCarPosition;
    private Piece previousPiece;
    private Piece currentPiece;
    private double distanceToNextPiece;
    private Car car;

    final DummyLogger logger = DummyLogger.getInstance();

    public RacingCar(PlayerId id) {
        this.car = new Car(id);
    }

    // getry na to co je v car position
    // TODO je to treba?
    public PlayerId getCarId() {
        return car.getId();
    }

    public Piece getCurrentPiece() {
        return currentPiece;
    }

    public CarPosition getCurrentCarPosition() {
        return currentCarPosition;
    }

    /**
     * Calculates the current car speed based on the distance passed from the
     * last position
     * 
     * @return
     */
    public double currentSpeed() {
        double speed = 0;
        // start of the race, no previous position
        if (previousCarPosition == null) {
            return speed;
        }

        int currentPieceIndex = currentCarPosition.getPiecePosition().getPieceIndex();
        int previousPieceIndex = previousCarPosition.getPiecePosition().getPieceIndex();
        double currentInPieceDistance = currentCarPosition.getPiecePosition().getInPieceDistance();
        double previousInPieceDistance = previousCarPosition.getPiecePosition().getInPieceDistance();
        // is still on the same piece
        if (currentPieceIndex == previousPieceIndex) {
            speed = currentInPieceDistance - previousInPieceDistance;
            return speed;
        }
        // is on the next piece
        double previousPieceLength = previousPiece.getLaneLength(currentCarPosition.getPiecePosition().getLane().getStartLaneIndex());
        speed = previousPieceLength - previousInPieceDistance + currentInPieceDistance;
        logger.info(" previousPieceIndex " + previousPieceIndex + " straight " + previousPiece.isStraight());
        logger.info(" currentPieceIndex " + currentPieceIndex + " straight " + currentPiece.isStraight());
        logger.info(" previousPieceLength " + previousPieceLength + " previousInPieceDistance " + previousInPieceDistance + " currentInPieceDistance "
                + currentInPieceDistance);
        return speed;
    }

    public double deltaAngle() {
        // start of the race, no previous position
        if (previousCarPosition == null) {
            return 0;
        }
        return currentCarPosition.getAngle() - previousCarPosition.getAngle();
    }

    public void setNewPosition(CarPosition currentCarPosition, Piece currentPiece) {
        this.previousCarPosition = this.currentCarPosition;
        this.previousPiece = this.currentPiece;
        this.currentPiece = currentPiece;
        this.currentCarPosition = currentCarPosition;
    }

}
