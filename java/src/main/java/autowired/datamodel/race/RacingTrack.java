package autowired.datamodel.race;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import autowired.datamodel.track.TrackLane;
import autowired.datamodel.track.Piece;
import autowired.datamodel.track.PiecePosition;
import autowired.datamodel.track.Track;
import autowired.logging.DummyLogger;
import autowired.message.data.Turbo;
import autowired.message.data.TurboAvailable;

public class RacingTrack {

    public static final int NUMBER_OF_CAR_POSITIONS_FOR_CALIBRATION = 4;

    private Track track;
    private RacingCar car;
    private double lowestOfMaxSafeSpeedsOnTrack;
    private int laps;

    // calculated parameters of the track
    // coefficient of power
    private double pow;
    // maximal speed achievable on this track
    private double vmax;
    // acceleration
    private double acc;
    // braking coefficient
    private double cob;
    // indicates if the parameters of the track are calculated yet
    private boolean parametersCalculated;

    // parameters variants
    private double accPow = acc * pow;
    private double oneMinusAcc = 1 - acc;

    private List<CarPosition> paramsSource = new ArrayList<>();

    final DummyLogger logger = DummyLogger.getInstance();

    private TurboAvailable turbo;

    private boolean turboInUse;

    private boolean crash;

    public RacingTrack(Track track, RacingCar car, int laps) {
        this.track = track;
        this.car = car;
        this.laps = laps;
        this.lowestOfMaxSafeSpeedsOnTrack = findTheWorstBand();
        calculateLaneLengthOnPieces();
    }

    private void calculateLaneLengthOnPieces() {
        for (Piece piece : track.getPieces()) {
            piece.initialize(track.getLanes(), laps);
        }
    }

    private double findTheWorstBand() {
        double min = 1;
        int nextPieceIndex = 1;
        for (Piece piece : track.getPieces()) {
            for (TrackLane lane : track.getLanes()) {
                // 60 is the max angle when car crashes
                double max = piece.getMaximalSafeSpeed(lane, track.getPieces(), nextPieceIndex - 1);
                if (max < min) {
                    min = max;
                }
            }
            nextPieceIndex++;
            if (nextPieceIndex >= track.getPieces().length) {
                nextPieceIndex -= track.getPieces().length;
            }

        }
        return min;

    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public RacingCar getCar() {
        return car;
    }

    public Piece getPiece(PiecePosition piecePosition) {
        return track.getPieces()[piecePosition.getPieceIndex()];
    }

    public Piece getPiece(int pieceIndex) {
        return track.getPieces()[pieceIndex];
    }

    public double getLowestOfMaxSafeSpeedsOnTrack() {
        return lowestOfMaxSafeSpeedsOnTrack;
    }

    public double getPow() {
        return pow;
    }

    public double getVmax() {
        return vmax;
    }

    public double getAcc() {
        return acc;
    }

    public int getParamsSourceSize() {
        return paramsSource.size();
    }

    private int getPiecesCount() {
        return track.getPieces().length;
    }

    private int getCurrentPieceIndex() {
        return car.getCurrentCarPosition().getPiecePosition().getPieceIndex();
    }

    public boolean isParametersCalculated() {
        return parametersCalculated;
    }

    public double laneLengthOnBandPiece(Piece piece, TrackLane lane) {
        int radius = piece.getRadius() + lane.getDistanceFromCenter();
        double length = Math.PI * radius / 180 * piece.getAngle();
        return length;
    }

    /**
     * @param startingSwitchIndex
     *            piece index of the starting switch
     * @param lane
     * @return
     */
    public double laneLengthBetweenNextSwitches(int startingSwitchIndex, TrackLane lane) {
        // TODO how the length on switch in band is calculated?
        double length = 0;
        for (int i = startingSwitchIndex; i < track.getPieces().length; i++) {
            Piece piece = track.getPieces()[i];

            // we got to the next switch
            if (i != startingSwitchIndex && piece.isSwitch()) {
                break;
            }

            length += piece.getLaneLength(lane.getIndex());

            // end of lap-start of lap: reset index
            if (i == track.getPieces().length - 1) {
                i = 0;
            }
        }
        return length;
    }

    public void calculateTrackParameters(CarPosition carPosition) {
        if (paramsSource.size() < NUMBER_OF_CAR_POSITIONS_FOR_CALIBRATION) {
            paramsSource.add(carPosition);
            if (logger.isLoggingInfo()) {
                logger.info(paramsSource.size() + " adding first position: " + new Date().getTime());
            }
        }
        if (paramsSource.size() == NUMBER_OF_CAR_POSITIONS_FOR_CALIBRATION) {
            double v1 = calculateSpeed(paramsSource.get(0), paramsSource.get(1));
            double v2 = calculateSpeed(paramsSource.get(1), paramsSource.get(2));
            double v3 = calculateSpeed(paramsSource.get(2), paramsSource.get(3));
            double v1_squared = v1 * v1;
            pow = v1_squared / (2 * v1 - v2);
            vmax = v1_squared / (2 * v1 - v2);
            acc = (2 * v1 - v2) / v1;
            cob = (v2 - v3) / v2;
            parametersCalculated = true;

            if (logger.isLoggingInfo()) {
                logger.info("v1:" + v1);
                logger.info("v2:" + v2);
                logger.info("v3:" + v3);
                logger.info("pow:" + pow);
                logger.info("vmax:" + vmax);
                logger.info("acc:" + acc);
                logger.info("cob:" + cob);
            }
        }
    }

    private double calculateSpeed(CarPosition startPiece, CarPosition endPiece) {
        if (endPiece.getPiecePosition().getPieceIndex() == startPiece.getPiecePosition().getPieceIndex()) {
            return endPiece.getPiecePosition().getInPieceDistance() - startPiece.getPiecePosition().getInPieceDistance();
        }
        // is on the next piece
        return track.getPieces()[startPiece.getPiecePosition().getPieceIndex()].getLength() - startPiece.getPiecePosition().getInPieceDistance()
                + endPiece.getPiecePosition().getInPieceDistance();
    }

    /**
     * Calculates braking distance with throtle=0
     * 
     * @param currentSpeed
     * @param targetSpeed
     * @return
     */
    public double calculateBrakingDistance(double currentSpeed, double targetSpeed) {
        double v1 = currentSpeed;
        double distance = 0;
        while (v1 > targetSpeed) {
            v1 = v1 * (1 - cob);
            distance += v1;
        }

        return distance;
    }

    /**
     * Calculates nextSpeed with throttle
     * 
     * @param currentSpeed
     * @param targetSpeed
     * @return
     */
    public double calculateNextSpeedWithAcceleration(double currentSpeed, double throttle) {
        return throttle * accPow + currentSpeed * oneMinusAcc;
    }

    public void setTurboAvailable(TurboAvailable turbo) {
        this.turbo = turbo;
    }

    public TurboAvailable getTurboAvailable() {
        return turbo;
    }

    public boolean isTurboInUse() {
        return turboInUse;
    }

    public void setTurboInUse(boolean turboInUse) {
        this.turboInUse = turboInUse;
    }

    public void setCrash(boolean b) {
        crash = b;
    }

    public boolean isCrash() {
        return crash;
    }

}
