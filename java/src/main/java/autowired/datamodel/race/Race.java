package autowired.datamodel.race;

import autowired.datamodel.car.Car;
import autowired.datamodel.track.Track;

public class Race {
    private Track track;
    private Car[] cars;
    private RaceSession raceSession;

    public Track getTrack() {
        return track;
    }

    public Car[] getCars() {
        return cars;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

}
