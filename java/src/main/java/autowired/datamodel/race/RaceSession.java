package autowired.datamodel.race;

public class RaceSession {
    private int laps;
    private long maxLapTimeMs;
    private boolean quickRace;

    public int getLaps() {
        return laps;
    }

    public long getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }

}
