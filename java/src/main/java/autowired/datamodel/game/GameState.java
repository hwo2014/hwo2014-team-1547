package autowired.datamodel.game;

import autowired.datamodel.race.RacingCar;

public class GameState {

    private Long gameTick;
    private String gameId;
    private boolean disqualified;
    private RacingCar racingCar;

    public void setGameTick(Long gameTick) {
        this.gameTick = gameTick;
    }

    public Long getGameTick() {
        return gameTick;
    }

    public void updateGameTick(Long gameTick) {
        if (gameTick == null) {
            return;
        }
        // this causes wrong game tick after qualification
        // if (this.gameTick == null || this.gameTick < gameTick) {
            this.gameTick = gameTick;
        // }
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return gameId;
    }

    public void gameFinished() {
        gameTick = null;
    }

    public void disqualified() {
        disqualified = true;
    }

    public boolean isDisqualified() {
        return disqualified;
    }

    public void setRacingCar(RacingCar racingCar) {
        this.racingCar = racingCar;
    }

    public RacingCar getRacingCar() {
        return racingCar;
    }

}
