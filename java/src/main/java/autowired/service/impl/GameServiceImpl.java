package autowired.service.impl;

import com.google.common.base.Preconditions;

import autowired.bot.StartUpParameters;
import autowired.datamodel.race.BotId;
import autowired.message.MessageData;
import autowired.message.data.CreateRace;
import autowired.message.data.Join;
import autowired.message.data.JoinRace;
import autowired.service.GameService;

public class GameServiceImpl implements GameService {

    private final BotId botId;
    private final String trackName;
    private final String gamePassword;
    private final int carCount;

    public GameServiceImpl(StartUpParameters parameters) {
        this.botId = new BotId(parameters.getBotName(), parameters.getBotKey());
        this.trackName = parameters.geTrackName();
        this.gamePassword = "AUTO-WIRED-TEST";
        this.carCount = parameters.getCarCount();
    }

    public MessageData createGame(int newCarCount) {
        Preconditions.checkNotNull(trackName, "Cannot create new race without track name");
        return new CreateRace(botId, trackName, gamePassword, newCarCount);
    }

    public MessageData createGame() {
        return createGame(carCount);
    }

    public MessageData getGameJoinMessage() {
        return getGameJoinMessage(botId.getName());
    }

    public MessageData getGameJoinMessage(String botName) {
        if (trackName == null) {
            return new Join(botName, botId.getKey());
        }
        BotId newBotId = new BotId(botName, botId.getKey());
        return new JoinRace(newBotId, trackName, gamePassword, carCount);
    }

}
