package autowired.service.impl;

import java.io.IOException;

import autowired.datamodel.game.GameState;
import autowired.message.MessageData;
import autowired.message.MessageDataTranslator;
import autowired.message.MessageWrapper;
import autowired.message.data.Ping;
import autowired.service.CarService;
import autowired.service.ServerConnector;
import autowired.service.strategy.CarControlStrategy;
import autowired.service.strategy.CarControlStrategyMapper;
import autowired.service.strategy.impl.DummyControlStrategyMapper;

public class DummyCarService implements CarService {

    private static final Ping PING = new Ping();

    private final ServerConnector serverConnector;
    private final GameState gameState;
    private final MessageDataTranslator translator;
    private final CarControlStrategyMapper strategyMapper;

    public DummyCarService(ServerConnector serverConnector) {
        this.serverConnector = serverConnector;
        this.gameState = new GameState();
        this.translator = new MessageDataTranslator();
        this.strategyMapper = new DummyControlStrategyMapper();
    }

    @Override
    public void race(final MessageData joinMessage) throws IOException {

        serverConnector.send(translator.packMessage(joinMessage));
        MessageWrapper inMessage = null;
        MessageData outMessage = null;

        while ((inMessage = serverConnector.recieve()) != null && !gameState.isDisqualified()) {
            gameState.updateGameTick(inMessage.getGameTick());
            CarControlStrategy strategy = strategyMapper.getStrategy(inMessage);
            outMessage = strategy.execute(inMessage, gameState);
            //send reply if expected to avoid disqualification
            if (outMessage == null && gameState.getGameTick() != null) {
                outMessage = PING;
            }
            if (outMessage != null) {
                serverConnector.send(translator.packMessage(outMessage, gameState));
            }
        }
    }

}
