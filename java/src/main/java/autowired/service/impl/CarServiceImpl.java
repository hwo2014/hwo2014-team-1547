package autowired.service.impl;

import java.io.IOException;

import autowired.logging.DummyLogger;

import autowired.datamodel.car.PlayerId;
import autowired.datamodel.game.GameState;
import autowired.datamodel.race.CarPosition;
import autowired.datamodel.race.RacingCar;
import autowired.datamodel.race.RacingTrack;
import autowired.datamodel.track.Direction;
import autowired.datamodel.track.Piece;
import autowired.datamodel.track.Track;
import autowired.datamodel.track.TrackLane;
import autowired.message.MessageData;
import autowired.message.MessageDataTranslator;
import autowired.message.MessageWrapper;
import autowired.message.data.GameInit;
import autowired.message.data.LapFinished;
import autowired.message.data.Ping;
import autowired.message.data.SwitchLane;
import autowired.message.data.Throttle;
import autowired.message.data.Turbo;
import autowired.message.data.TurboAvailable;

import autowired.service.CarService;
import autowired.service.ServerConnector;

public class CarServiceImpl implements CarService {

    private static final Ping PING = new Ping();
    private final DummyLogger logger = DummyLogger.getInstance();

    private final ServerConnector serverConnector;
    private final GameState gameState;
    private final MessageDataTranslator translator;

    public CarServiceImpl(ServerConnector serverConnector) {
        this.serverConnector = serverConnector;
        this.gameState = new GameState();
        this.translator = new MessageDataTranslator();
    }

    @Override
    public void race(final MessageData joinMessage) throws IOException {

        serverConnector.send(translator.packMessage(joinMessage));
        CarPosition carPosition = null;
        RacingTrack track = null;
        RacingCar car = null;
        MessageWrapper inMessage = null;
        MessageData outMessage = null;
        while ((inMessage = serverConnector.recieve()) != null) {

            gameState.updateGameTick(inMessage.getGameTick());
            outMessage = null;

            switch (inMessage.getMessageType()) {

            case JOIN:
                System.out.println("Joined");
                break;

            case GAMWE_END:
                System.out.println("Race end");
                break;

            case YOUR_CAR:
                car = translator.toRacingCar(inMessage);
                gameState.setGameId(inMessage.getGameId());
                break;

            case GAME_START:
                System.out.println("Race start. GameTick " + gameState.getGameTick());
                outMessage = PING;
                break;

            case CAR_POSITIONS:
                if (gameState.getGameTick() != null) {
                    carPosition = translator.toCarPosition(inMessage, car);
                    if (!track.isParametersCalculated()) {
                        track.calculateTrackParameters(carPosition);
                    }
                    car.setNewPosition(carPosition, track.getPiece(carPosition.getPiecePosition()));
                    try {
                        outMessage = calculateAction(track);
                    } catch (Throwable e) {
                        System.out.println("in message " + inMessage);
                        e.printStackTrace();
                    }
                }
                break;

            case GAME_INIT:
                track = translator.toRacingTrack(inMessage, car);
                outMessage = calculateAction(track);
                System.out.println("number of laps: " + ((GameInit) inMessage.getData()).getRace().getRaceSession().getLaps());
                System.out.println("Race init");
                break;

            case LAP_FINISHED:
                LapFinished lapFinished = (LapFinished)inMessage.getData();
                if (lapFinished.getCar().getColor().equals(car.getCarId().getColor())) {
                    System.out.println("Finished lap #");
                }
                break;

            case FINISH:
                gameState.gameFinished();
                System.out.println("Game finished");
                break;

            case TURBO_AVAILABLE:
                if (!track.isCrash()) {
                    System.out.println("Turbo available");
                    TurboAvailable turbo = (TurboAvailable) inMessage.getData();
                    track.setTurboAvailable(turbo);
                }
                break;

            case TURBO_START:
                System.out.println("Turbo start");
                PlayerId turboStart = (PlayerId) inMessage.getData();
                if (track.getCar().getCarId().equals(turboStart)) {
                    track.setTurboInUse(true);
                }
                break;

            case TURBO_END:
                System.out.println("Turbo end");
                PlayerId turboEnd = (PlayerId) inMessage.getData();
                if (track.getCar().getCarId().equals(turboEnd)) {
                    track.setTurboInUse(false);
                }
                break;

            case CRASH:
                System.out.println("CRASH");
                PlayerId crash = (PlayerId) inMessage.getData();
                if (track.getCar().getCarId().equals(crash)) {
                    track.setCrash(true);
                }
                break;

            case SPAWN:
                System.out.println("SPAWN");
                PlayerId spawn = (PlayerId) inMessage.getData();
                if (track.getCar().getCarId().equals(spawn)) {
                    track.setCrash(false);
                }
                break;

            case UNKNOWN:
                System.out.println("Unknown message: " + inMessage.getMsgType());
                logger.info("Unknown message: " + inMessage.getMsgType());
                outMessage = PING;
                break;

            default:
                outMessage = PING;
                break;
            }
            if (outMessage != null) {
                serverConnector.send(translator.packMessage(outMessage, gameState));
            }
        }
    }

    private MessageData calculateAction(RacingTrack track) {
        // first 3 ticks are used for calculation of parameters
        // twice accelerate then once break
        if (!track.isParametersCalculated()) {
            if (track.getParamsSourceSize() < RacingTrack.NUMBER_OF_CAR_POSITIONS_FOR_CALIBRATION - 1) {
                return new Throttle(1);
            }
            if (track.getParamsSourceSize() == RacingTrack.NUMBER_OF_CAR_POSITIONS_FOR_CALIBRATION - 1) {
                return new Throttle(0);
            }
        }

        // race runs
        Direction direction = switchLane(track);
        if (direction != null) {
            return new SwitchLane(direction);
        }
        // use turbo?
        if (shouldUseTurbo(track)) {
            System.out.println("Turbo");
            return new Turbo("VRRRRRRRRRRRRRRRRRRR!");
        }
        return new Throttle(calculateThrottle(track));
    }

    private boolean shouldUseTurbo(RacingTrack track) {
        if (track.getTurboAvailable() == null || track.isTurboInUse() || track.isCrash()) {
            return false;
        }
        double rovinka = calculatestraightPartLength(track);
        double turboDistance = track.getVmax() * track.getTurboAvailable().getTurboDurationTicks();
        logger.info("rovinka " + rovinka + " turboDistance " + turboDistance);
        if (rovinka > turboDistance) {
            track.setTurboAvailable(null);
            return true;
        }
        return false;

        }

    private double calculatestraightPartLength(RacingTrack track) {
        int currentPieceIndex = track.getCar().getCurrentCarPosition().getPiecePosition().getPieceIndex();
        
        int currentLaneIndex = track.getCar().getCurrentCarPosition().getPiecePosition().getLane().getEndLaneIndex();
        Piece[] pieces = track.getTrack().getPieces();
        double currentPieceLength = pieces[currentPieceIndex].getLaneLength(currentLaneIndex );
        double rovinka = currentPieceLength -track.getCar().getCurrentCarPosition().getPiecePosition().getInPieceDistance();
        int checkedPieceIndex = Piece.settlePieceIndex(currentPieceIndex, pieces);
        Piece checkedPiece = pieces[checkedPieceIndex];
        while (checkedPiece.isStraight()) {
            rovinka+=checkedPiece.getLaneLength(currentLaneIndex);
            checkedPieceIndex = Piece.settlePieceIndex(checkedPieceIndex + 1, pieces);
            checkedPiece = pieces[checkedPieceIndex];
        }
        return rovinka;
    }

    private Direction switchLane(RacingTrack track) {
        int currentPieceIndex = track.getCar().getCurrentCarPosition().getPiecePosition().getPieceIndex();
        int currentLaneIndex = track.getCar().getCurrentCarPosition().getPiecePosition().getLane().getEndLaneIndex();

        Piece nextPiece = track.getTrack().getPiece(currentPieceIndex + 1);
        if (nextPiece.isSwitch()) {
            int firstSwitchIndex = currentPieceIndex + 1;
            int secondSwitchIndex = firstSwitchIndex;
            if (logger.isLoggingInfo()) {
                logger.info("nextPiece " + firstSwitchIndex + " is Switch");
            }
            secondSwitchIndex++;
            Piece nextSwitch = track.getTrack().getPiece(secondSwitchIndex);
            while (!nextSwitch.isSwitch()) {
                secondSwitchIndex++;
                nextSwitch = track.getTrack().getPiece(secondSwitchIndex);
            }
            logger.info("nextSwitch " + secondSwitchIndex);
            double minDistance = Double.MAX_VALUE;
            int shortestLaneIndex = 0;
            for (TrackLane lane : track.getTrack().getLanes()) {
                double distance = calculateSwitcSwitchDistance(track.getTrack(), firstSwitchIndex, secondSwitchIndex, lane.getIndex());
                if (minDistance >= distance) {
                    minDistance = distance;
                    shortestLaneIndex = lane.getIndex();
                    // TODO dont swich if the same length as current lane
                    
                }
            }
            if (logger.isLoggingInfo()) {
                logger.info("shortestLane " + shortestLaneIndex + " currentLane " + currentLaneIndex);
                logger.info("alreadyChanged " + nextPiece.isLaneChanged(track.getCar().getCurrentCarPosition().getPiecePosition().getLap()));
            }
            if (shortestLaneIndex == currentLaneIndex || nextPiece.isLaneChanged(track.getCar().getCurrentCarPosition().getPiecePosition().getLap())) {
                return null;
            } else {
                nextPiece.setLaneChanged(track.getCar().getCurrentCarPosition().getPiecePosition().getLap());
                if (currentLaneIndex < shortestLaneIndex) {
                    logger.info(currentPieceIndex + " right");
                    return Direction.RIGHT;
                } else {
                    logger.info(currentPieceIndex + " left");
                    return Direction.LEFT;
                }
            }
        }

        return null;
    }

    private double calculateSwitcSwitchDistance(Track track, int firstSwitchIndex, int nextSwitchIndex, int laneIndex) {
        double switchSwitchDistance = 0;
        for (int i = firstSwitchIndex; i <= nextSwitchIndex; i++) {
            switchSwitchDistance += track.getPiece(i).getLaneLength(laneIndex);
        }
        return switchSwitchDistance;
    }

    private double calculateThrottle(RacingTrack track) {

        int currentPieceIndex = track.getCar().getCurrentCarPosition().getPiecePosition().getPieceIndex();
        double currentSpeed = track.getCar().currentSpeed();
        double maxBrakingDistance = track.calculateBrakingDistance(currentSpeed, track.getLowestOfMaxSafeSpeedsOnTrack());
        int currentEndLaneIndex = track.getCar().getCurrentCarPosition().getPiecePosition().getLane().getEndLaneIndex();
        TrackLane currentEndLane = track.getTrack().getLanes()[currentEndLaneIndex];

        // calculate possible braking in pieces in maxBreakingDistance

        double distanceToEndOfPiece = track.getTrack().getPieces()[track.getCar().getCurrentCarPosition().getPiecePosition().getPieceIndex()]
                .getLaneLength(currentEndLaneIndex)
                - track.getCar().getCurrentCarPosition().getPiecePosition().getInPieceDistance();
        double distance = distanceToEndOfPiece;
        int i = currentPieceIndex;
        boolean brake = false;
        while (distance < maxBrakingDistance) {
            if (i >= track.getTrack().getPieces().length) {
                i = i - track.getTrack().getPieces().length;
            }
            Piece nextPiece = track.getPiece(i);
            Piece checkedPiece = nextPiece;

            double brakingDistance = track.calculateBrakingDistance(checkedPiece.getMaximalSafeSpeed(currentEndLane, track.getTrack().getPieces(), i),
                    currentSpeed);
            logger.info("piece " + i + "brakingDistance " + brakingDistance);
            // double safetyAdding = 50;// TODO calculate it somehow
            double distanceToPiece = distanceToEndOfPiece;
            for (int j = currentPieceIndex; j <= i; j++) {
                distanceToPiece += track.getPiece(j).getLaneLength(currentEndLaneIndex);
            }
            if (checkedPiece.getMaximalSafeSpeed(currentEndLane, track.getTrack().getPieces(), i) < currentSpeed &&
                    brakingDistance < distanceToPiece) {
                brake = true;
            }
            distance += checkedPiece.getLaneLength(currentEndLaneIndex);
            i++;
        }

        double throttle = brake ? 0 : 1;
        if (logger.isLoggingInfo()) {
            logger.info("currentPieceIndex " + currentPieceIndex + " currentSpeed " + currentSpeed + " throttle " + throttle);
        }
        return throttle;
    }

}
