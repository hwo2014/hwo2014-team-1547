package autowired.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import autowired.logging.DummyLogger;
import autowired.message.MessageWrapper;
import autowired.message.MessageWrapperDeserializer;
import autowired.service.ServerConnector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BlockingSocketServerConnector implements ServerConnector {

    private final BufferedReader reader;
    private final PrintWriter writer;
    private final Socket socket;
    private final Gson gson;
    private final DummyLogger logger = DummyLogger.getInstance();

    public BlockingSocketServerConnector(String hostName, int port) throws UnsupportedEncodingException, IOException {
        this.socket = new Socket(hostName, port);
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MessageWrapper.class, new MessageWrapperDeserializer());
        this.gson = gsonBuilder.create();
    };

    @Override
    public void send(MessageWrapper message) throws IOException {
        if (message == null) {
            return;
        }
        String line = gson.toJson(message);
        if (line != null) {
            logger.trace(line);
            writer.println(line);
            writer.flush();
        }
    }

    @Override
    public MessageWrapper recieve() throws IOException {
        MessageWrapper message = null;
        String line = reader.readLine();
        if (line != null) {
            logger.debug(line);
            message = gson.fromJson(line, MessageWrapper.class);
        }
        return message;
    }

    @Override
    public void close() throws IOException {
        if (socket != null && !socket.isClosed()) {
            socket.close();
        }
    }

    @Override
    public void finalize() throws IOException {
        close();
    }

}
