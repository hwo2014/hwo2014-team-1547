package autowired.service;

import autowired.message.MessageData;

public interface GameService {

    public MessageData createGame();
    public MessageData createGame(int newCarCount);
    public MessageData getGameJoinMessage();
    public MessageData getGameJoinMessage(String botName);

}
