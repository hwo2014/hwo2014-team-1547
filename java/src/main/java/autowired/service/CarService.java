package autowired.service;

import java.io.IOException;

import autowired.message.MessageData;

public interface CarService {
    public void race(final MessageData joinMessage) throws IOException;
}
