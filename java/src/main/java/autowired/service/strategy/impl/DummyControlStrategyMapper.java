package autowired.service.strategy.impl;

import static autowired.message.MessageType.*;

import com.google.common.collect.ImmutableMap;

import autowired.message.MessageDataTranslator;
import autowired.message.MessageType;
import autowired.message.MessageWrapper;
import autowired.service.strategy.CarControlStrategy;
import autowired.service.strategy.CarControlStrategyMapper;

public class DummyControlStrategyMapper implements CarControlStrategyMapper {

    private static final CarControlStrategy DEFAULT_STRATEGY = new DefaultCarControlStrategy();
    private static final CarControlStrategy ECHO_STRATEGY = new EchoCarControlStrategy();

    private static final MessageDataTranslator translator = new MessageDataTranslator();
    private static final ImmutableMap<MessageType, CarControlStrategy> STRATEGIES = new ImmutableMap.Builder<MessageType, CarControlStrategy>()
            .put(CAR_POSITIONS, new DummyCarSpeedStrategy())
            .put(CRASH, ECHO_STRATEGY)
            .put(DNF, new CarDisqualifiedStrategy())
            .put(GAME_INIT, ECHO_STRATEGY)
            .put(GAME_START, new DummyGameStartStrategy(translator))
            .put(LAP_FINISHED, ECHO_STRATEGY)
            .put(TOURNAMENT_END, ECHO_STRATEGY)
            .put(YOUR_CAR, new CreateRacingCarStrategy(translator))
            .put(UNKNOWN, ECHO_STRATEGY)
            .build();

    @Override
    public CarControlStrategy getStrategy(MessageWrapper inputMessage) {
        CarControlStrategy result = STRATEGIES.get(inputMessage.getMessageType());
        return result == null ? DEFAULT_STRATEGY : result;
    }

}
