package autowired.service.strategy.impl;

import autowired.datamodel.game.GameState;
import autowired.message.MessageData;
import autowired.message.MessageDataTranslator;
import autowired.message.MessageWrapper;
import autowired.service.strategy.CarControlStrategy;

public class CreateRacingCarStrategy implements CarControlStrategy {

    private final MessageDataTranslator translator;

    public CreateRacingCarStrategy(MessageDataTranslator translator) {
        this.translator = translator;
    }

    @Override
    public MessageData execute(MessageWrapper inputMessage, GameState gameState) {
        gameState.setRacingCar(translator.toRacingCar(inputMessage));
        gameState.setGameId(inputMessage.getGameId());
        return NO_RESPONSE;
    }

}
