package autowired.service.strategy.impl;

import autowired.datamodel.game.GameState;
import autowired.message.MessageData;
import autowired.message.MessageWrapper;
import autowired.service.strategy.CarControlStrategy;

public class CarDisqualifiedStrategy implements CarControlStrategy {

    @Override
    public MessageData execute(MessageWrapper inputMessage, GameState gameState) {
        System.out.println(inputMessage.getMsgType());
        gameState.disqualified();
        return NO_RESPONSE;
    }

}
