package autowired.service.strategy.impl;

import autowired.datamodel.game.GameState;
import autowired.message.MessageData;
import autowired.message.MessageDataTranslator;
import autowired.message.MessageWrapper;
import autowired.message.data.Throttle;
import autowired.service.strategy.CarControlStrategy;

public class DummyGameStartStrategy implements CarControlStrategy {

    private final MessageDataTranslator translator;

    public DummyGameStartStrategy(MessageDataTranslator translator) {
        this.translator = translator;
    }

    @Override
    public MessageData execute(MessageWrapper inputMessage, GameState gameState) {
        System.out.println(inputMessage.getMsgType());
        double throttle = 1.0;
        return new Throttle(throttle);
    }

}
