package autowired.service.strategy.impl;

import autowired.datamodel.game.GameState;
import autowired.message.MessageData;
import autowired.message.MessageWrapper;
import autowired.message.data.Throttle;
import autowired.service.strategy.CarControlStrategy;

public class DummyCarSpeedStrategy implements CarControlStrategy {

    public DummyCarSpeedStrategy() {
        
    }

    @Override
    public MessageData execute(MessageWrapper inputMessage, GameState gameState) {
        return new Throttle(0.65);
    }

}
