package autowired.service.strategy;

import autowired.message.MessageWrapper;

public interface CarControlStrategyMapper {

    public CarControlStrategy getStrategy(MessageWrapper inputMessage);

}
