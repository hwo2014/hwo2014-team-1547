package autowired.service.strategy;

import autowired.datamodel.game.GameState;
import autowired.message.MessageData;
import autowired.message.MessageWrapper;

public interface CarControlStrategy {

    public static final MessageData NO_RESPONSE = null;

    public MessageData execute(MessageWrapper inputMessage, GameState gameState);

}
