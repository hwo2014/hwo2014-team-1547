package autowired.service;

import java.io.IOException;

import autowired.message.MessageWrapper;

public interface ServerConnector extends AutoCloseable {

    public void send(MessageWrapper message) throws IOException;
    public MessageWrapper recieve() throws IOException;

}
