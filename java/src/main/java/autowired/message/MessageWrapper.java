package autowired.message;

/**
 * Common message wrapper for both in-bound
 * and out-bound game messages.
 */
public class MessageWrapper {

    private String msgType;
    private Object data;
    private String gameId;
    private Long gameTick;

    public MessageWrapper() {}

    private MessageWrapper(Builder builder) {
        this.msgType = builder.msgType;
        this.data = builder.data;
        this.gameId = builder.gameId;
        this.gameTick = builder.gameTick;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Long getGameTick() {
        return gameTick;
    }

    public void setGameTick(Long gameTick) {
        this.gameTick = gameTick;
    }

    public MessageType getMessageType() {
        return MessageType.fromString(msgType);
    }

    public static class Builder {
        private String msgType;
        private Object data;
        private String gameId;
        private Long gameTick;

        public static Builder withMsgType(String msgType) {
            Builder builder = new Builder();
            builder.msgType = msgType;
            return builder;
        }

        public static Builder withData(MessageData messageData) {
            Builder builder = new Builder();
            if (messageData != null) {
                builder.msgType = messageData.getMessageType().getText();
                builder.data = messageData.getData();
            }
            return builder;
        }

        public Builder withData(Object data) {
            this.data = data;
            return this;
        }

        public Builder withGameId(String gameId) {
            this.gameId = gameId;
            return this;
        }

        public Builder withGameTick(Long gameTick) {
            this.gameTick = gameTick;
            return this;
        }

        public MessageWrapper build() {
            return new MessageWrapper(this);
        }
    }

}
