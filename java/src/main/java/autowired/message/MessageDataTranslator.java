package autowired.message;

import autowired.datamodel.car.PlayerId;
import autowired.datamodel.game.GameState;
import autowired.datamodel.race.CarPosition;
import autowired.datamodel.race.RacingCar;
import autowired.datamodel.race.RacingTrack;
import autowired.message.data.CarPositions;
import autowired.message.data.GameInit;
import autowired.message.data.YourCar;

public class MessageDataTranslator {

    public MessageWrapper packMessage(MessageData messageData, GameState gameState) {
        if (messageData == null) {
            return null;
        }
        return MessageWrapper.Builder
                .withData(messageData)
                .withGameId(gameState.getGameId())
                .withGameTick(gameState.getGameTick())
                .build();
    }

    public MessageWrapper packMessage(MessageData messageData) {
        if (messageData == null) {
            return null;
        }
        return MessageWrapper.Builder
                .withData(messageData)
                .build();
    }

    public RacingCar toRacingCar(MessageWrapper messageWrapper) {
        YourCar yourCar = (YourCar)messageWrapper.getData();
        PlayerId playerId = new PlayerId(yourCar.getName(), yourCar.getColor());
        return new RacingCar(playerId);
    }

    public CarPosition toCarPosition(MessageWrapper messageWrapper, RacingCar car) {
        CarPositions carPositions = (CarPositions)messageWrapper.getData();
        for (CarPosition position: carPositions.getCarPositions()) {
            if(car.getCarId().equals(position.getId())){
                return position;
            }
        }
        return null;
    }

    public RacingTrack toRacingTrack(MessageWrapper messageWrapper, RacingCar car) {
        GameInit gameInit = (GameInit)messageWrapper.getData();
        return new RacingTrack(gameInit.getRace().getTrack(), car, gameInit.getRace().getRaceSession().getLaps());
    }

}
