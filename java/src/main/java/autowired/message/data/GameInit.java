package autowired.message.data;

import autowired.datamodel.race.Race;
import autowired.message.MessageData;

public class GameInit extends MessageData {

    private Race race;

    public Race getRace() {
        return race;
    }

}
