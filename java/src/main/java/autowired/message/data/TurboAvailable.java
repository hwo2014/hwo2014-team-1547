package autowired.message.data;

import autowired.message.MessageData;

public class TurboAvailable extends MessageData {

    private Double turboDurationMilliseconds;
    private Integer turboDurationTicks;
    private Double turboFactor;

    public TurboAvailable() {}

    public TurboAvailable(Double turboDurationMilliseconds, Integer turboDurationTicks, Double turboFactor) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
        this.turboDurationTicks = turboDurationTicks;
        this.turboFactor = turboFactor;
    }

    public Double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    public Integer getTurboDurationTicks() {
        return turboDurationTicks;
    }

    public Double getTurboFactor() {
        return turboFactor;
    }

}
