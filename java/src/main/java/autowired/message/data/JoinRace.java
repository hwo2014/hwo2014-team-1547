package autowired.message.data;

import autowired.datamodel.race.BotId;

public class JoinRace extends AdvancedRace {

    public JoinRace() {}

    public JoinRace(BotId botId, String trackName, String password, Integer carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

}
