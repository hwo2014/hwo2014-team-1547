package autowired.message.data;

import autowired.datamodel.car.PlayerId;
import autowired.message.MessageData;

public class Dnf extends MessageData {

    private PlayerId car;
    private String reason;

    public Dnf() {}

    public Dnf(PlayerId car, String reason) {
        super();
        this.car = car;
        this.reason = reason;
    }

    public PlayerId getCar() {
        return car;
    }
    public String getReason() {
        return reason;
    }

}
