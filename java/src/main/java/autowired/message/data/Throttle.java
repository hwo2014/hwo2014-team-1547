package autowired.message.data;

import autowired.message.MessageData;

public class Throttle extends MessageData {

    private double value;

    public Throttle() {}

    public Throttle(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    public Object getData() {
        return value;
    }

}
