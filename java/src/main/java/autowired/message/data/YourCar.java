package autowired.message.data;

public class YourCar extends CarInfoMessage {

    public YourCar() {}

    public YourCar(String name, String color) {
        this.name = name;
        this.color = color;
    }

}
