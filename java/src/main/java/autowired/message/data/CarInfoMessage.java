package autowired.message.data;

import autowired.message.MessageData;

public abstract class CarInfoMessage extends MessageData {

    protected String name;
    protected String color;

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

}
