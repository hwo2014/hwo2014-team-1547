package autowired.message.data;

import autowired.message.MessageData;

public class Join extends MessageData {

    private String name;
    private String key;

    public Join() {}

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

}
