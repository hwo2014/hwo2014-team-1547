package autowired.message.data;

import autowired.message.MessageData;

public class Turbo extends MessageData {

    private String message;

    public Turbo() {}

    public Turbo(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public Object getData() {
        return message;
    }

}
