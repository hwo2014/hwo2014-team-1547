package autowired.message.data;

public class Spawn extends CarInfoMessage {

    public Spawn() {}

    public Spawn(String name, String color) {
        this.name = name;
        this.color = color;
    }

}
