package autowired.message.data;

public class Finish extends CarInfoMessage {

    public Finish() {}

    public Finish(String name, String color) {
        this.name = name;
        this.color = color;
    }

}
