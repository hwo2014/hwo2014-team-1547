package autowired.message.data;

import autowired.datamodel.track.Direction;
import autowired.message.MessageData;

public class SwitchLane extends MessageData {

    private Direction direction;

    public SwitchLane() {}

    public SwitchLane(Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public Object getData() {
        return direction != null ? direction.getText() : null;
    }

}
