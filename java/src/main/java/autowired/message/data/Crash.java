package autowired.message.data;

public class Crash extends CarInfoMessage {

    public Crash() {}

    public Crash(String name, String color) {
        this.name = name;
        this.color = color;
    }

}
