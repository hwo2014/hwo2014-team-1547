package autowired.message.data;

import autowired.datamodel.race.BotId;
import autowired.message.MessageData;

public abstract class AdvancedRace extends MessageData {

    protected BotId botId;
    protected String trackName;
    protected String password;
    protected Integer carCount;

    public void setBotId(BotId botId) {
        this.botId = botId;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCarCount(Integer carCount) {
        this.carCount = carCount;
    } 

}
