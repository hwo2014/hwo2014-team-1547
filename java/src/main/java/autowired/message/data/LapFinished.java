package autowired.message.data;

import autowired.datamodel.car.PlayerId;
import autowired.message.MessageData;

public class LapFinished extends MessageData {

    private PlayerId car;
    private Object lapTime;
    private Object raceTime;
    private Object ranking;

    public PlayerId getCar() {
        return car;
    }

    public Object getLapTime() {
        return lapTime;
    }

    public Object getRaceTime() {
        return raceTime;
    }

    public Object getRanking() {
        return ranking;
    }

}
