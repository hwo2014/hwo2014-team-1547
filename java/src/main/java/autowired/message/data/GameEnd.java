package autowired.message.data;

import autowired.datamodel.race.RaceResult;
import autowired.message.MessageData;

public class GameEnd extends MessageData {

    private RaceResult[] results;

    public RaceResult[] getResults() {
        return results;
    }

}
