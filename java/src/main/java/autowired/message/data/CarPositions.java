package autowired.message.data;

import autowired.datamodel.race.CarPosition;
import autowired.message.MessageData;
import autowired.message.MessageType;

public class CarPositions extends MessageData {

    private CarPosition[] carPositions;

    public CarPositions() {}

    public CarPositions(CarPosition[] carPositions) {
        this.carPositions = carPositions;
    }

    public CarPosition[] getCarPositions() {
        return carPositions;
    }

    @Override
    public Object getData() {
        return this.carPositions;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.forClass(carPositions.getClass());
    }

}
