package autowired.message;

import java.lang.reflect.Type;

import autowired.datamodel.race.CarPosition;
import autowired.message.data.CarPositions;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * Custom JSON deserializer for unmarshaling of the "data" part of messages.
 *
 */
public class MessageWrapperDeserializer implements JsonDeserializer<MessageWrapper> {

    private static final String DATA_ATTRIBUTE_NAME = "data";

    @Override
    public MessageWrapper deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        Gson gson = new Gson();
        MessageWrapper wrapper = gson.fromJson(jsonObject, MessageWrapper.class);
        MessageType messageType = wrapper.getMessageType();
        if (MessageType.UNKNOWN == messageType) {
            System.out.println(messageType + " : " + jsonObject.toString());
        }
        Object data = null;// TODO handle if it is primitive
        if (jsonObject.get(DATA_ATTRIBUTE_NAME) != null && !jsonObject.get(DATA_ATTRIBUTE_NAME).isJsonPrimitive()) {
            data = context.deserialize(jsonObject.get(DATA_ATTRIBUTE_NAME), messageType.getClazz());
        }
        //some messages need special treatment
        switch (messageType) {
        case CAR_POSITIONS:
            data = new CarPositions((CarPosition[])data);
            break;
        default:
            //pass trough
        }
        wrapper.setData(data);
        return wrapper;
    }

}
