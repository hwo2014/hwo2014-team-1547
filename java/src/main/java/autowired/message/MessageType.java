package autowired.message;

import autowired.datamodel.car.PlayerId;
import autowired.datamodel.race.CarPosition;
import autowired.message.data.*;

/**
 * Enumeration with all known messages
 */
public enum MessageType {
    CAR_POSITIONS("carPositions", CarPosition[].class),
    CRASH("crash", PlayerId.class),
    CREATE_RACE("createRace", CreateRace.class),
    DNF("dnf", Dnf.class),
    FINISH("finish", Finish.class),
    GAME_INIT("gameInit", GameInit.class),
    GAMWE_END("gameEnd", GameEnd.class),
    GAME_START("gameStart", GameStart.class),
    JOIN("join", Join.class),
    JOIN_RACE("joinRace", JoinRace.class),
    LAP_FINISHED("lapFinished", LapFinished.class),
    PING("ping", Ping.class),
    SPAWN("spawn", PlayerId.class),
    SWITCH_LANE("switchLane", SwitchLane.class),
    THROTTLE("throttle", Throttle.class),
    TOURNAMENT_END("tournamentEnd", TournamentEnd.class),
    TURBO("turbo", Turbo.class),
    TURBO_AVAILABLE("turboAvailable", TurboAvailable.class),
    YOUR_CAR("yourCar", YourCar.class),
    TURBO_START("turboStart", PlayerId.class),
    TURBO_END("turboEnd", PlayerId.class),
    UNKNOWN("Unknown message type", MessageData.class);

    private String text;
    private Class<?> clazz;

    MessageType(String text, Class<?> clazz) {
        this.text = text;
        this.clazz = clazz;
    }

    public String getText() {
        return this.text;
    }

    public Class<?> getClazz() {
        return this.clazz;
    }

    public static MessageType fromString(String text) {
        if (text != null) {
            for (MessageType msg : MessageType.values()) {
                if (text.equalsIgnoreCase(msg.text)) {
                    return msg;
                }
            }
        }
        return UNKNOWN;
    }

    public static MessageType forClass(Class<?> clazz) {
        if (clazz != null) {
            for (MessageType msg : MessageType.values()) {
                if (clazz.equals(msg.clazz)) {
                    return msg;
                }
            }
        }
        return UNKNOWN;
    }

}
