package autowired.message;

/**
 * Common parent of all "data" messages 
 */
public class MessageData {

    public Object getData() {
        return this;
    }

    public MessageType getMessageType() {
        return MessageType.forClass(this.getClass());
    }

}
