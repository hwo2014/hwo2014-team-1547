package autowired.bot;

import autowired.logging.DummyLogger;
import autowired.message.MessageData;
import autowired.service.CarService;
import autowired.service.GameService;
import autowired.service.ServerConnector;
import autowired.service.impl.BlockingSocketServerConnector;
import autowired.service.impl.DummyCarService;
import autowired.service.impl.GameServiceImpl;

public class MultiTrackSlave {

    public static void main(final String[] args) {

        final DummyLogger logger = DummyLogger.getInstance();
        final StartUpParameters parameters = StartUpParameters.parseFromCommandLine(args);

        try (ServerConnector serverConnector = new BlockingSocketServerConnector(parameters.getHost(), parameters.getPort())) {
            logger.info(parameters.toString());
            GameService gameService = new GameServiceImpl(parameters);
            MessageData gameJoin = gameService.getGameJoinMessage();
            CarService carService = new DummyCarService(serverConnector);
            carService.race(gameJoin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.shutdown();
    }

}
