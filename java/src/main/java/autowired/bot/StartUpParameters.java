package autowired.bot;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public class StartUpParameters {

    private static final int ARGS_COUNT = 4;
    private static final int HOST_INDEX = 0;
    private static final int PORT_INDEX = 1;
    private static final int BOT_NAME_INDEX = 2;
    private static final int BOT_KEY_INDEX = 3;
    private static final int TRACK_NAME_INDEX = 4;
    private static final int CAR_COUNT_INDEX = 5;

    private final String host;
    private final int port;
    private final String botName;
    private final String botKey;
    private final String trackName;
    private final int carCount;

    private StartUpParameters(String host, int port, String botName, String botKey, String trackName, Integer carCount) {
        this.host = host;
        this.port = port;
        this.botName = botName;
        this.botKey = botKey;
        this.trackName = trackName;
        this.carCount = carCount == null ? 1 : carCount.intValue();
    }

    public static StartUpParameters parseFromCommandLine(String[] args) {
        Preconditions.checkArgument(args != null && args.length >= ARGS_COUNT, "Expected 4 command line parameters, recieved less.");

        String host = args[HOST_INDEX];
        int port = Integer.parseInt(args[PORT_INDEX]);
        String botName = args[BOT_NAME_INDEX];
        String botKey = args[BOT_KEY_INDEX];
        String trackName = args.length > TRACK_NAME_INDEX ? args[TRACK_NAME_INDEX] : null;
        Integer carCount = args.length > CAR_COUNT_INDEX ? new Integer(args[CAR_COUNT_INDEX]) : null;

        Preconditions.checkArgument(carCount == null || carCount > 0, "Invalid number of cars: " + carCount);

        return new StartUpParameters(host, port, botName, botKey, trackName, carCount);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getBotName() {
        return botName;
    }

    public String getBotKey() {
        return botKey;
    }

    public String geTrackName() {
        return trackName;
    }

    public int getCarCount() {
        return carCount;
    }

    @Override
    public String toString() {
       return Objects.toStringHelper(this)
               .omitNullValues()
               .add("host", host)
               .add("port", port)
               .add("botName", botName)
               .add("botKey", botKey)
               .add("trackName", trackName)
               .add("carCount", carCount)
               .toString();
    }

}
