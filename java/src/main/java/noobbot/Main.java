package noobbot;

import autowired.bot.StartUpParameters;
import autowired.logging.DummyLogger;
import autowired.message.data.Join;
import autowired.service.CarService;
import autowired.service.ServerConnector;
import autowired.service.impl.BlockingSocketServerConnector;
import autowired.service.impl.CarServiceImpl;

public class Main {

    public static void main(final String[] args) {
        final DummyLogger logger = DummyLogger.getInstance();
        final StartUpParameters parameters = StartUpParameters.parseFromCommandLine(args);
        try (ServerConnector serverConnector = new BlockingSocketServerConnector(parameters.getHost(), parameters.getPort())) {
            logger.info(parameters.toString());
            CarService service = new CarServiceImpl(serverConnector);
            service.race(new Join(parameters.getBotName(), parameters.getBotKey()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.shutdown();
    }

}
